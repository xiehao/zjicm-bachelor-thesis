# CUZ NMC Bachelor Thesis LaTeX Template

This is the LaTeX template of Bachelor thesis for Communication University of Zhejiang, with the latest formatting requirements satisfied.

## Dependencies

### Tex Live

The newest version of TeX Live is recommended, and the compilers XeLaTeX+BibTeX are preferred for Unicode support. The installation guide could be found throughout the Internet.

On Linux (Arch Linux for example):

``` shell
sudo pacman -S texlive-most texlive-langchinese
```

On Mac (using Homebrew, with cask installed, is preferred):

``` shell
brew cask install mactex
```

On Windows:

Just search the latest TeX Live `.iso` file and install.

### Pygmentize

Since the template uses `minted` package, a Python environment with `pygments` installed should be ready. When Python is ready, type the following command:

```bash
pip install pygments
```

## Fonts

If you have Dual OSes (Arch Linux and Windows), just execute `./install-fonts.sh` to copy and install the required fonts into Linux from a Windows partition.

If you have a Mac, things could be more complicated, you need to install some Windows fonts through Homebrew.

## Build

In the sub-directories, simply execute `make` to build.

## Clean

In the sub-directories, simply execute `make clean` to remove all temporary files.

## Thanks

Thanks to Zhang Hai and his [LaTeX template](https://github.com/DreaminginCodeZH/zju-csse-undergraduate-design-latex-template)